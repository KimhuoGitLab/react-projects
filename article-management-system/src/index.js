import React from 'react';
import ReactDOM from 'react-dom';
import {Header} from './main-components/Header';
import {Body} from './main-components/Body';
import {Footer} from './main-components/Footer';
import App from './App';
import 'bootstrap/dist/css/bootstrap.css';

ReactDOM.render(
    <Header />, 
    document.getElementById('header')
);

ReactDOM.render(
    <Body />, 
    document.getElementById('body')
);

// ReactDOM.render(
//     <Footer />, 
//     document.getElementById('footer')
// );