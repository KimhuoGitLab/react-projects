import React, { Component } from 'react';
import {NavLink} from "react-router-dom";
import { Table } from 'reactstrap';
import {ROOT_URL} from './../Utilities';

export class Article extends Component {
    state = {
        article : {},
        shouldUpdate : false
    }


    fetchArticle = (id) => {
        fetch(ROOT_URL+"/v1/api/articles/"+id).then((response)=>{
            response.json().then((res_body)=>{
                this.setState({
                    article : res_body.DATA,
                    shouldUpdate : true
                })
            });
        });
    }

    constructor(props){
        super(props)
        this.fetchArticle(props.match.params.article_id);
    }

    shouldComponentUpdate(nextProps, nextState){
        console.log("BEFORE RECEIVE PROPS")
        return false
    }

    render() {
        return (
            <Table>
        <thead>
          <tr>
            <th>Title</th>
            <th>Create Date</th>
            <th>Author</th>
          </tr>
        </thead>
        <tbody>
            {console.log("RENDER", this.state.article.AUTHOR)}
            <tr key={this.state.article.ID}>
                <td>{this.state.article.TITLE}</td>
                <td>{this.state.article.CREATED_DATE}</td>
                <td>this.state.article.AUTHOR.NAME</td>
            </tr>
        </tbody>
      </Table>
        );
    }
}
