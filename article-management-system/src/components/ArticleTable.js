import React, { Component } from 'react';
import {NavLink} from "react-router-dom";
import { Table } from 'reactstrap';
import {ROOT_URL} from './../Utilities';

export class ArticleTable extends Component {
    state = {
        articles : []
    }


    fetchArticles = (id) => {
        var url = (id ? ROOT_URL + ('/v1/api/categories/'+id+'/articles') : (ROOT_URL + '/v1/api/articles'));
        fetch(url).then((response)=>{
            return response.json();
        }).then((res_body)=>{
            this.setState({
                articles : res_body.DATA
            })
        });
    }

    componentWillReceiveProps(props){
        this.fetchArticles(props.match.params.category_id);
    }

    render() {
        return (
            <Table>
        <thead>
          <tr>
            <th>#</th>
            <th>Title</th>
            <th>Create Date</th>
            <th>Author</th>
          </tr>
        </thead>
        <tbody>
            {this.state.articles.map((article, index)=>{
                return(
                    <tr key={article.ID}>
                        <th scope="row">{index+1}</th>
                        <td><NavLink to={"/article/"+article.ID}>{article.TITLE}</NavLink></td>
                        <td>{article.CREATED_DATE}</td>
                        <td>{article.AUTHOR.NAME}</td>
                    </tr>
                )})
            }
        </tbody>
      </Table>
        );
    }
}
