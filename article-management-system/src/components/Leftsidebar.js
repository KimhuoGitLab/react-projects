import React from "react";
import {NavLink} from "react-router-dom";

export class Leftsidebar extends React.Component {
    render() {
        return (
            <nav className="navbar navbar-default sidebar" role="navigation">
                <div className="container-fluid">
                    <div className="navbar-collapse" id="bs-sidebar-navbar-collapse-1">
                        <ul className="nav navbar-nav">
                            {this.props.categories.map((category)=>{
                                return(
                                    <li key={category.ID}>
                                        <NavLink to={"/category/"+category.ID}>
                                            {category.NAME}
                                        </NavLink>
                                    </li>
                                );
                            })}
                        </ul>
                    </div>
                </div>
            </nav>
        )
    }
}