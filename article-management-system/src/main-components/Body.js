import React from "react";
import {Leftsidebar} from './../components/Leftsidebar';
import {ROOT_URL} from './../Utilities';
import {BrowserRouter, Switch, Route} from 'react-router-dom'
import {ArticleTable} from './../components/ArticleTable'
import {Article} from './../components/Article'

import App from './../App'

export class Body extends React.Component {
    state = {
        categories : []
    }


    fetchCategoryies = () => {
        fetch(ROOT_URL + '/v1/api/categories').then((response)=>{
            return response.json();
        }).then((res_body)=>{
            this.setState({
                categories : res_body.DATA
            })
        });
    }

    constructor(){
        super();
        this.fetchCategoryies();
    }

    render (){
        return (
            <BrowserRouter>
                <div id="page-wrapper">
                    <div className="container-fluid">
                        <div className="row">
                            <div className="col-lg-3">
                                <Leftsidebar categories={this.state.categories} />
                            </div>
                            <div className="col-lg-9">
                                    <Switch>
                                        <Route exact path="/" component={ArticleTable} />
                                        <Route path="/category/:category_id" component={ArticleTable} />
                                        <Route path="/article/:article_id" component={Article} />
                                    </Switch>
                            </div>
                        </div>
                    </div>
                </div>
            </BrowserRouter>

        )
    }
}