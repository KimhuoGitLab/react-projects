import React from "react";

export class Header extends React.Component {
    render() {
        return (
            <nav className="navbar navbar-default navbar-static-top navbar-expand-md navbar-dark bg-dark " role="navigation">
                <a className="navbar-brand" href="#">ARTICLE MANAGEMENT SYSTEM</a>

                <div className="collapse navbar-collapse" id="navbarsExampleDefault">
                    <ul className="navbar-nav mr-auto">
                        <li className="nav-item active">
                            <a className="nav-link" href="#">Home
                                <span className="sr-only">(current)</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </nav>

        )
    }
}