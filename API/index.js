import { AppRegistry } from 'react-native';
import App from './NasaAPI';

AppRegistry.registerComponent('API', () => App);
