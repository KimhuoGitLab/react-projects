import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View
} from 'react-native';

import api from './utilities/api';

export default class App extends Component {

  constructor(props){
    super(props);

    this.state = {
      rovers: [],
      roverName: ''
    }
  }

  componentDidMount(){
    api.getRovers().then((res) => {
      this.setState({
        rovers: res.rovers,
        roverName: res.rovers[0].name
      })
    });
  }

  render() {
    console.log("Rovers: ", this.state.rovers);

    return (
      <View style={styles.container}>
        <Text style={styles.welcome}>
          Rovers!
        </Text>
        <Text style={styles.instructions}>
          Rovers: {this.state.roverName}
        </Text>

      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});
