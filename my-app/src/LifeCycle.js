import React from 'react';
import Counter from './Counter';

export default class App extends React.Component {

   constructor(props) {
      super(props);
		
      this.state = {
         ShowHide: false,
         data: 0,
         textColor: "primary"
      }

      this.setNewNumber = this.setNewNumber.bind(this)
   };

   setNewNumber() {
        this.state.ShowHide?this.setState({data: this.state.data + 1},
            this.setColor.bind(this)):this.setState({data: this.state.data})
   }

   toggleShowHide(){
       this.setState({
           ShowHide:!this.state.ShowHide
       })
   }

   setColor(){
       if(this.state.data > 24){
            this.setState({
                textColor: "primary"
            })
       }else if(this.state.data > 19){
            this.setState({
                textColor: "success"
            })
       }else if(this.state.data > 14){
            this.setState({
                textColor: "info"
            })
        }else if(this.state.data > 9){
            this.setState({
                textColor: "warning"
            })
       }else if(this.state.data > 4){
            this.setState({
                textColor: "danger"
            })
        }
   }

   render() {
      return (
         <div className="text-center">
             <div className="btn-group text-center">
                <h1>React App Counter</h1>
                <button className="btn btn-default btn-block btn-lg" onClick = {this.toggleShowHide.bind(this)}>
                    {this.state.ShowHide?"HIDE":"SHOW"}</button>
                <button className="btn btn-primary btn-block btn-lg" onClick = {this.setNewNumber}>INCREASE</button>
                <h1 className={"text-"+ this.state.textColor}>{this.state.ShowHide?<Counter value={this.state.data}/>:null}</h1>
            </div>
         </div>
      );
    }
}
