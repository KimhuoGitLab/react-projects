import React from "react";
import {Switch, Route} from  "react-router-dom";

import {Home} from "./../pages/Home";
import {User} from "./../pages/User";
export class Body extends React.Component{
    render(){
        return(
            <Switch>
                <Route exact path = '/' component = {Home}/>
                <Route path = '/home' component = {Home}/>
                <Route path = '/user/:id' component = {User}/>
            </Switch>
        );
    }
}
