import React from 'react';

import {Header} from "./Header";
import {Body} from "./Body";

export class Root extends React.Component{
    render(){
        return(
            <div className="container">
                <div>
                    <Header/>
                </div>
                <hr/>
                <div>
                    <Body/>
                </div>
            </div>
        );
    }
}
