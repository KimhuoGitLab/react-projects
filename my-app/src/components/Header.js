import React from 'react';
import {NavLink} from "react-router-dom";

export const Header = (props) => {
    return(
        <nav className="navbar navbar-default">
            <div className="container">
                <div className="navbar-header">
                    <ul className="nav navbar-nav">
                        <li><NavLink to={"/home"} activeStyle={{color: "blue"}}>Home</NavLink></li>
                        <li><NavLink to={"/user/10"} activeStyle={{color: "blue"}}>User 10</NavLink></li>
                        <li><NavLink to={"/user/22"} activeStyle={{color: "blue"}}>User 22</NavLink></li>

                        {/* <li><NavLink to={"/user/10"} activeClassName={"active"}>Home</NavLink></li>
                        <li><NavLink to={"/user/22"} activeClassName={"active"}>Home</NavLink></li> */}
                    </ul>
                </div>
            </div>
        </nav>
    );
}
