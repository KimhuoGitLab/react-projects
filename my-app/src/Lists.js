import React from 'react';

export default class App extends React.Component{
    constructor(props){
        super(props);

        this.state = {
            fruits:
            [
                {
                    name: 'Apple',
                    id: 1
                },

                {
                    name: 'Orange',
                    id: 2
                },

                {
                    name: 'Banana',
                    id: 3
                },

                {
                    name: 'Coconut',
                    id: 4
                },

                {
                    name: 'Banana 2',
                    id: 3
                },
            ]
        }
    };

    render(){
        return(
            <div className="container text-left">
            <hr/>
               <div>
                   {this.state.fruits.map((currentFruit) =>
                        <div key={currentFruit.id}>
                            <div>{currentFruit.id} - {currentFruit.name}</div>
                        </div>
                   )}
               </div>
            </div>
        );
    }
};
