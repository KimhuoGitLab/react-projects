import React from 'react';
import ReactDOM from 'react-dom';
import App from './Lists';

ReactDOM.render(
    <App />,
    document.getElementById('root')
);
