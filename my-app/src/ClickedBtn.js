import React from 'react';
import ReactDOM from 'react-dom';

class App extends React.Component{
    render(){
        return(
            <div>
                <Header/>
                <Content/>
                <Button/>
            </div>
        );
    }
}

class Header extends React.Component{
    render(){
        return(
            <div>
                <h1>Header</h1>
            </div>
        );
    }
}

class Content extends React.Component{
    render(){
        return(
            <div>
                <h2>Content</h2>
                <p>The content text!</p>
            </div>
        );
    }
}

class Button extends React.Component {
    constructor(){
        super();
        this.state = {
            count : 0,
        };
    }
    updateCount(){
        this.setState((prevState, props) => {
            return{count : prevState.count +1}
        });
    }
    render(){
        return(<button
            onClick = {() => this.updateCount()} >
            Clicked : {this.state.count} times
        </button>);
    }
}

export default App;

ReactDOM.render(
    <App />, 
    document.getElementById('root')
);
