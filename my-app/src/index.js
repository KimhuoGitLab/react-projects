import React from "react";
import ReactDOM from 'react-dom';
import { render } from "react-dom";
import { BrowserRouter, Rout } from "react-router-dom";

import { Root } from "./components/Root";

class App extends React.Component {
    render() {
        return ( <
            BrowserRouter >
            <
            Root / >
            <
            /BrowserRouter>
        );
    }
}

ReactDOM.render( <
    App / > ,
    document.getElementById('root')
);