import React from 'react';
import ReactDOM from 'react-dom';

export default class App extends React.Component{
    constructor(props){
        super(props);

        this.state = {
            data: ''
        }

        this.updateState = this.updateState.bind(this);
        this.clearInput = this.clearInput.bind(this);
    };

    updateState(e){
        this.setState({data: e.target.value});
    }

    clearInput(){
        this.setState({data: ''});
        ReactDOM.findDOMNode(this.refs.myInput).focus();
    }

    render(){
        return(
            <div className="container text-center">
                <hr/>
                <input value = {this.state.data}
                    onChange = {this.updateState}
                    ref = "myInput">
                </input><br/><hr/>
                <button className="btn btn-success btn-md" onClick = {this.clearInput}>CLEAR</button>
                <hr/>
            </div>
        );
    }
};
