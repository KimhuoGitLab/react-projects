import {
    StyleSheet
} from 'react-native';

export const Styles = StyleSheet.create({
    container: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: '#EEEEEE',         
      padding: 25,
      paddingTop: 0
    },
  
    welcome: {
      fontSize: 20,
      textAlign: 'center',
      margin: 10,
    },
  
    login: {
      textAlign: 'center',
      color: '#333333',
      marginBottom: 5,
    },
    inputBox: {
        width: '100%', 
        backgroundColor: '#FFFFFF',
        margin: 2
    },
    button: {
        marginTop: 4, 
        marginBottom: 10, 
        width: '100%',
        display: "flex"
    },
    signupText: {
        fontSize: 10,
        textDecorationLine: "underline",
        color: "rgb(33, 150, 243)"
    }
  });