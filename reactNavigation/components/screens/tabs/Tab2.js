/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  TouchableOpacity
} from 'react-native';
import {Styles} from './../../Styles'

export default class Welcome extends Component<{}> {
  render() {
    return (
      <View style={Styles.container}>
        <Text style={Styles.welcome}>
        THIS IS TAB 2
        </Text>
      </View>
    );
  }
}