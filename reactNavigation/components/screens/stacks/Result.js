import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  TouchableOpacity
} from 'react-native';
import {Styles} from './../../Styles'

export default class Welcome extends Component<{}> {
  render() {
    return (
      <View style={Styles.container}>
        <Text style={Styles.welcome}>
          WELCOME 
        </Text>
        <Text style={Styles.login}>
          User: {this.props.navigation.state.params.name}
        </Text>
        <Text style={Styles.login}>
          Password: {this.props.navigation.state.params.password}
        </Text>
      </View>
    );
  }
}