import React, { Component } from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    Image,
    TextInput,
    Button,
    TouchableOpacity
} from 'react-native';

import { Styles } from './../../Styles'

export default class Login extends Component {

    goToSignup = () => {
        this.props.navigation.navigate('Signup')
    }

    doLogin = () => {
        this.props.navigation.navigate('Result', {name: this.state.name, password: this.state.password})
    }

    
    setName = (value) => {
        this.setState({name: value});
    }

    setPassword = (value) => {
        this.setState({password: value});
    }

    state = {
        name : "",
        password : ""
    }

    render() {
        return (
            <View style={Styles.container}>
                {/* <Text style={styles.title}>KNONG DAI</Text> */}
                <TextInput style={Styles.inputBox} underlineColorAndroid={'#FFFFFF'} placeholder="Username" placeholderStyle={{ fontStyle: 'italic' }} 
                    onChangeText={this.setName}
                />
                <TextInput style={Styles.inputBox} underlineColorAndroid={'#FFFFFF'} placeholder="Password" secureTextEntry={true} placeholderStyle={{ fontStyle: 'italic' }} 
                    onChangeText={this.setPassword}
                />
                <View style={Styles.button}>
                    <Button title="LOG IN" onPress={this.doLogin} />
                </View>
                <TouchableOpacity onPress={this.goToSignup} >
                    <Text style={Styles.signupText}>SIGN UP</Text>
                </TouchableOpacity>
            </View>
        );
    }
}
