import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  TouchableOpacity
} from 'react-native';
import {Styles} from './../../Styles'

export default class Signup extends Component<{}> {
  render() {
    return (
      <View style={Styles.container}>
        <Text style={Styles.welcome}>
          This is signup page
        </Text>
        <TouchableOpacity>
          <Text style={Styles.login}>
              Click here to go back
          </Text>
        </TouchableOpacity>
      </View>
    );
  }
}