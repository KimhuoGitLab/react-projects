import React, { Component } from 'react';
import {
  View,
  Text
} from 'react-native';
import { StackNavigator } from 'react-navigation';
import Login from './../screens/stacks/Login';
import Signup from './../screens/stacks/Signup';
import Result from './../screens/stacks/Result';


export default StackNavigation = StackNavigator({
  Login: {
    screen: Login,
  },
  Signup: {
    screen: Signup,
  },
  Result: {
    screen: Result,
  },
});
