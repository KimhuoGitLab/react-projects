import React, { Component } from 'react';
import { TabNavigator } from 'react-navigation';
import Tab1 from './../screens/tabs/Tab1/';
import Tab2 from './../screens/tabs/Tab2/';


export default TabNavigation = TabNavigator({
  "Tab 1": {
    screen: Tab1,
  },
  "Tab 2": {
    screen: Tab2,
  },
});
