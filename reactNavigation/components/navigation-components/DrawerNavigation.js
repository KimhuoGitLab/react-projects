import React, { Component } from 'react';
import {
  View
} from 'react-native';
import { DrawerNavigator } from 'react-navigation';
import Welcome from './../screens/Welcome/';
import TabNavigation from './TabNavigation/';
import StackNavigation from './StackNavigation/';


export default DrawerNavigation = DrawerNavigator({
    "Welcome":{
      screen: Welcome   // End-Point Component
    },
    "Tab Navigation": {
      screen: TabNavigation   // End-Point Component
    },
    "Stack Navigation": {
      screen: StackNavigation // End-Point Component
    }

  });
