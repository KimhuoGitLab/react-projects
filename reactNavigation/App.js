/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  TouchableOpacity
} from 'react-native';
import DrawerNavigation from './components/navigation-components/DrawerNavigation'


export default class App extends Component<{}> {
  render() {
    return (
      <DrawerNavigation />
    );
  }
}
