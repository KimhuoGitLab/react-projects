import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  ListView,
  TouchableHighlight,

} from 'react-native';

import styles from './styles';

import * as firebase from 'firebase';

// Initialize Firebase
const firebaseConfig = {
  apiKey: "AIzaSyDBKkuFrfAqaQmmi_sUQzjxElG_rl1kiRM",
  authDomain: "react-native141217.firebaseapp.com",
  databaseURL: "https://react-native141217.firebaseio.com",
  projectId: "react-native141217",
  storageBucket: "react-native141217.appspot.com",
};


const firebaseApp = firebase.initializeApp(firebaseConfig);

const itemsRef = firebase.database().ref('items');


export default class App extends Component<{}> {

  constructor() {
    super();
    const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
    this.state = {
      dataSource: ds.cloneWithRows(['row 1', 'row 2']),
    };
    
    itemsRef.set({
      name: 'Kimhuo',
      email: 'Kimhuo99@gmail.com',
      age: 20,
      address: 'PP'
    })
  }

  /* listenForItems(itemsRef) {
    itemsRef.on('value', (snap) => {

      // get children as an array
      const items = [];
      snap.forEach((child) => {
        items.push({
          title: child.val().title,
          _key: child.key
        });
      });

      this.setState({
        dataSource: this.state.dataSource.cloneWithRows(items)
      });

    });
  } */

  /* componentDidMount() {
    this.listenForItems(this.itemsRef);
  } */

  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.welcome}>
          Welcome to React Native - Firebase App!
        </Text>

        <ListView
          dataSource={this.state.dataSource}
          renderRow={(rowData) => <Text>{rowData}</Text>}
        />

      </View>
    );
  }
}
