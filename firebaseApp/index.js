import { AppRegistry } from 'react-native';
import App from './FirebaseApp';

AppRegistry.registerComponent('firebaseApp', () => App);
