import React, {Component} from 'react';
import {
  FlatList,
  Text,
  StyleSheet,
  View,

} from 'react-native';

export default class App extends Component {

  dataList = [
    {
      id: 1,
      name: 'Kimhuo'
    },
    {
      id: 2,
      name: 'Kimhay'
    }
  ]

  render(){
    return(
      <View contentContainerStyle={styles.container}>
        <FlatList
          data={this.dataList}
          renderItem={({item})=> <Text>{item.id+'-'+item.name}</Text>}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignContent: 'center',
  }
});