/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  FlatList,

} from 'react-native';

/* const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' +
    'Cmd+D or shake for dev menu',
  android: 'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
}); */

export default class App extends Component {

  dataList = [
    {
      id: '1',
      name: 'A',
    },
    {
      id: '2',
      name: 'B'
    }
  ]

  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.welcome}>
          Welcome to React Native!
        </Text>
        <Text style={styles.instructions}>
          Section List Demo App
        </Text>
        {/* <Text style={styles.instructions}>
          {instructions}
        </Text> */}

        <FlatList
          data={this.dataList}
          // ItemSeparatorComponent={()=> (<View style={{borderWidth: 1}}/>)}
          renderItem={({item})=> <Text>{item.id+'-'+item.name}</Text>}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    // justifyContent: 'center',
    // alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});
