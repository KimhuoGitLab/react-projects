import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import registerServiceWorker from './registerServiceWorker';
import Body from "./classes/body"
import 'bootstrap/dist/css/bootstrap.css';

ReactDOM.render(<Body />, document.getElementById('root'));
registerServiceWorker();
