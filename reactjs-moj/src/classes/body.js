import React,{Component} from "react";
import {BrowserRouter,Switch,Route,NavLink} from "react-router-dom";
import Header from "./header/header";
import Footer from "./footer/footer";
import Officers,{OfficersPositions} from "./components/officers";
import Home from "./components/home";
// import Positions from "./components/positions"
import { Container, Row, Col } from 'reactstrap';
import Positions from "./components/officerposition"
import logo from "./header/MOJ.png"

class Body extends Component
{
    render()
    {
        return(
            <BrowserRouter>
            <div>
                <Header/>
                <Container>
                <Switch>
                    <Route exact path="/" component={Home} />
                    <Route path="/officers" component={Officers}/>
                    <Route path="/positions" component={Positions}/>
                    <Route path="/officerspositions" component={OfficersPositions}/>
                </Switch>
                </Container>
                <Footer/>
            </div>
            </BrowserRouter>
        );
    }
}
export default Body;