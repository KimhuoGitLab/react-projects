import './officerposition.css';
import React,{Component} from 'react';
import { Container, Row, Col ,Table} from 'reactstrap';
import {Modal, ModalHeader, ModalBody, ModalFooter,Input,Label} from 'reactstrap'
import swal from 'sweetalert'
import { Button } from 'reactstrap';


class PositionOfficer extends Component
{
    render()
    {
        return(
            <div>
                
            </div>
        );
    }
}

class Positions extends Component
{

    constructor()
    {
        super()
        this.state=
        {
            positions:''
        }
    }

    loadPositions()
    {
        let url='http://110.74.194.126:16020/api/judicial/positions';
        fetch(url)
        .then(function(response) {
        if (response.status >= 400) {
        throw new Error("Bad response from server");
        }
        return response.json();
        })
        .then((data)=>{
                this.setState(
                {
                    positions:data._embedded.positions
                }
            );
        });        
    }
    componentWillMount()
    {
        this.loadPositions();
    }
    render()
    {
        return(
            <div>
                <Row className="head-position">
                    <AddPositionForm/>
                </Row>
                {this.state.positions.length==0?'No Data Found':
                    this.state.positions.map((currentPosition)=>{
                        return (
                            <Position position={currentPosition}/>
                        )
                    })
                }
            </div>
        );
    }
}


class Position extends Component
{
    constructor(props)
    {
        super(props);
        this.state=
        {
            modal:false,
            isRemoved:false,
            title:this.props.position.title,
            description:this.props.position.description,
            link:this.props.position._links
        }
    }
    hidePosition()
    {
        this.setState(()=>{
            return {isRemoved:true}
        });
    }
    toggle() {
        this.setState({
          modal: !this.state.modal
        });
      }
    changeTitle(e)
    {
        let val=e.target.value;
        this.setState(()=>
        {
            return {
                title:val
            }
        });
    }
    changeDescrib(e)
    {
        let val=e.target.value;
        this.setState(()=>
        {
            return {
                description:val
            }
        });
    }
    deletePosition()
    {
        let url=this.state.link.self.href;
        console.log("Delete url",url);
        swal({
            title: "តើប្រាកដទេ?",
            text: "តើអ្នកប្រាកដថាចង់កែប្រែព័ត៌មានមែនឬ?",
            icon: "warning",
            buttons: true,
            dangerMode: true,
          })
          .then(willDelete => {
            if (willDelete) {
            
                fetch(url,{
                    method:'DELETE',
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json'
                    }
                })
                .then(function(response)
                {
                    swal("បានលុបរួចរាល់!", "ទិន្នន័យមុខតំណែង ត្រូវបានលុបរួចរាល់", "success");
                }
                );
                
                this.hidePosition();
            }
            else
            {
                swal("មិនបានលុប!", "ទិន្នន័យមុខតំណែង មិនត្រូវបានលុប", "warning");
            }
          });
    }
    updatePosition()
    {
        let url=this.state.link.self.href;
        console.log("Update url",url);
        let data=
        {
            title:this.state.title,
            description:this.state.description
        }
        swal({
            title: "តើប្រាកដទេ?",
            text: "តើអ្នកប្រាកដថាចង់កែប្រែព័ត៌មានមែនឬ?",
            icon: "warning",
            buttons: true,
            dangerMode: true,
          })
          .then(willUpdate => {
            if (willUpdate) {
            
                fetch(url,{
                    method:'PUT',
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json'
                    },
                    body:JSON.stringify(data)
                })
                .then(function(response)
                {
                    swal("បានកែប្រែរួចរាល់!", "ទិន្នន័យមុខតំណែង ត្រូវបានកែប្រែរួចរាល់", "success");
                }
                );
                
                
            }
            else
            {
                swal("មិនបានកែប្រែ!", "ទិន្នន័យមុខតំណែង មិនត្រូវបានកែប្រែ", "warning");
            }
            this.toggle();
          });
    }
    render()
    {
        return (
            <div>
            {this.state.isRemoved?null:
            <Row className="position-row">
                <Col>{this.state.title}</Col>
                <Col>{this.state.description}</Col>
                <Col>
                    <Button color='warning' onClick={()=>this.toggle()}>កែប្រែ</Button>
                    <Modal isOpen={this.state.modal} toggle={this.toggle} size="lg" className="modalform">
                        <ModalHeader toggle={this.toggle}>កែប្រែព័ត៌មានមុខតំណែង</ModalHeader>
                        <ModalBody>
                            <Table>
                                <tbody>
                                    <tr>
                                        <td><Label>តំណែង</Label></td>
                                        <td><Input type="text" value={this.state.title} onChange={(e)=>this.changeTitle(e)}/></td>
                                        <td><Label>ការបរិយាយ</Label></td>
                                        <td><Input type="text" value={this.state.description} onChange={(e)=>this.changeDescrib(e)}/></td>
                                    </tr> 
                                </tbody>
                            </Table>
                        </ModalBody>
                        <ModalFooter>
                            <Button color="primary"onClick={(e)=>{this.updatePosition()}}>កែប្រែព័ត៌មានមុខតំណែង</Button>{' '}
                            
                            <Button color="secondary" onClick={()=>this.toggle()}>បោះបង់</Button>
                        </ModalFooter>
                    </Modal>
                    {'   '}
                    <Button color='danger' onClick={()=>this.deletePosition()}>លុប</Button>
                </Col>
            </Row>
            }
            
            </div>
        );

    }


}

class AddPositionForm extends Component
{

    constructor(props)
    {
        super(props);
        this.state=
        {
            modal:false,
            title:'',
            description:''
        }
    }
    toggle() {
        this.setState({
          modal: !this.state.modal
        });
      }
    changeTitle(e)
    {
        let val=e.target.value;
        this.setState(()=>
        {
            return {
                title:val
            }
        });
    }
    changeDescrib(e)
    {
        let val=e.target.value;
        this.setState(()=>
        {
            return {
                description:val
            }
        });
    }
    savePosition()
    {
        let url='http://110.74.194.126:16020/api/judicial/positions';
        console.log("Update url",url);
        let data=
        {
            id:0,
            title:this.state.title.trim(),
            description:this.state.description.trim(),
        }
        swal({
            title: "តើប្រាកដទេ?",
            text: "តើអ្នកប្រាកដថាចង់កែប្រែព័ត៌មានមែនឬ?",
            icon: "warning",
            buttons: true,
            dangerMode: true,
          })
          .then(willDelete => {
            if (willDelete) {
            
                fetch(url,{
                    method:'POST',
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json'
                    },
                    body:JSON.stringify(data)
                })
                .then(function(response)
                {
                    swal("បានបន្ថែមរួចរាល់!", "ទិន្នន័យមុខតំណែង ត្រូវបានបន្ថែម", "success");
                    window.location.reload();
                }
                );
            
            }
            else
            {
                swal("មិនបានកែប្រែ!", "ទិន្នន័យមុខតំណែង មិនត្រូវបានកែប្រែ", "warning");
            }
            this.toggle();
          });
    }
    render()
    {
        return (
                <Col>
                    <Button color='primary' onClick={()=>this.toggle()}>បន្ថែមព័ត៌មានតំណែង</Button>
                    <Modal isOpen={this.state.modal} toggle={this.toggle} size="lg" className="modalform">
                        <ModalHeader toggle={this.toggle}>បន្ថែមព័ត៌មានមុខតំណែង</ModalHeader>
                        <ModalBody>
                            <Table>
                                <tbody>
                                    <tr>
                                        <td><Label>តំណែង</Label></td>
                                        <td><Input type="text" value={this.state.title} onChange={(e)=>this.changeTitle(e)}/></td>
                                        <td><Label>ការបរិយាយ</Label></td>
                                        <td><Input type="text" value={this.state.description} onChange={(e)=>this.changeDescrib(e)}/></td>
                                    </tr> 
                                </tbody>
                            </Table>
                        </ModalBody>
                        <ModalFooter>
                            <Button color="primary"onClick={(e)=>{this.savePosition()}}>រក្សាទុកព័ត៌មានមុខតំណែង</Button>{' '}
                            
                            <Button color="secondary" onClick={this.toggle}>បោះបង់</Button>
                        </ModalFooter>
                    </Modal>
                </Col>
        );
    }

}



export default Positions;