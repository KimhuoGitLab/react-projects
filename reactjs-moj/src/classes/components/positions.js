import React,{Component} from "react"
import { Table,Button, ModalHeader, ModalBody, ModalFooter,Modal,Form, FormGroup, Label, Input, FormText } from 'reactstrap';
import SweetAlert from 'sweetalert-react';
import swal from "sweetalert";
class Positions extends Component{

    render()
    {
        return(
            <div>
                <FormPositionModule/>
                <TablePosition />
            </div>
        );
    }
}

class TablePosition extends React.Component{
    constructor(props){
        super();
        this.state={
        positions:[]
        }
       // this.deletePosition=this.deletePosition.bind(url,this);
    }
    componentDidMount(){
        fetch('http://110.74.194.126:16020/api/judicial/positions?page=1&size=20&sort=5')
            .then(function(response) { 
                return response.json();
        }).then((position)=> {
            this.setState({positions:position._embedded.positions});
        });
    }
    render() {
        return (
           <div>
              <Table bordered>
                <thead>
                    <tr>
                    <th className="titles">Title</th>
                    <th>Description</th>
                    <th colSpan="2" className="action">Action</th>
                    </tr>
                 </thead>
                <tbody>
                    {this.state.positions.map((position, i) =>  
                      <TableRow key={i} data={position}/>)
                      }

                </tbody>
              </Table>
           </div>
        );
    }  
}


class TableRow extends React.Component {
  constructor(){
    super();
    this.state={
      show:false
    }
  }
  deletePosition(url){
    swal({
      title: "Delete",
      text: "Are you sure to delete ?",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
    .then((willDelete) => {
      if (willDelete) {
        fetch(url,{
          method: 'DELETE',
          headers: {"Content-Type": "application/json"},
        }).then((response)=> { 
            return response.status;
          })
        .then((data)=>console.log(data))
        .catch((err)=> 
          console.log('Request failure: ', err)
        );
        swal("Data has been deleted!", {
          icon: "success",
        });
      } else {
        swal("Data has not delete!");
      }
    });
  }
    render() {
       return (
           <tr>
           <td>{this.props.data.title}</td>
           <td>{this.props.data.description}</td>
           <td>
            <FormUpdatePositionModule data={this.props.data}/></td>
            <td><Button color="danger"  size="sm" onClick={()=>this.deletePosition(this.props.data._links.self.href)}>Delete</Button>{' '}
           </td>
         </tr>
       );
    }
}



class FormPositionModule extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
          modal: false,
          nestedModal: false,
          backdrop: false,
          postitle:"",
          postdescription:"",
        };
    
        this.toggle = this.toggle.bind(this);
        this.toggleNested = this.toggleNested.bind(this);
        this.addPosition=this.addPosition.bind(this);
      }
    
      toggle() {
        this.setState({
          modal: !this.state.modal,          
        });
      }
    
      toggleNested() {
        this.setState({
          nestedModal: !this.state.nestedModal
        });
      }
      addPosition(){
        swal({
          title: "Save",
          text: "Do you want to save?",
          icon: "warning",
          buttons: true,
          dangerMode: true,
        })
        .then((willDelete) => {
          if (willDelete) {
            fetch('http://110.74.194.126:16020/api/judicial/positions', {  
              method: 'POST',  
              headers: {  
                  'Accept': 'application/json',
                  'Content-Type': 'application/json'  
              },  
               body: JSON.stringify({
                  id:107,
                  title:this.state.postitle,
                  description:this.state.postdescription,
            })
          })
          .then((data)=>data.json())  
          .then((data)=>console.log(data))
          .catch((err)=> 
            console.log('Request failure: ', err)
          );
            this.setState({modal: false});
            this.setState({action:true})
          } else {
            swal("Data did not save");
            this.setState({modal: false});
          }
        });
  }

      render() {
        return (     
          <div>
            <Button color="success" size="sm" className="addnew" onClick={this.toggle}>AddNew </Button>
    
            <Modal isOpen={this.state.modal} toggle={this.toggle} className={this.props.className} className= "modal-lg" backdrop={this.props.backdrop}>
              <ModalHeader toggle={this.toggle}>Add Position</ModalHeader>
              <ModalBody>
              <Form>
                    <FormGroup>
                    <Label for="exampleText">Title</Label>
                    <Input type="text" name="title" id="txttitle" value={this.state.postitle} onChange={(e)=>{this.setState({postitle:e.target.value})}} placeholder="Title" />
                    </FormGroup>
                    <FormGroup>
                    <Label for="exampleText">Description</Label>
                    <Input type="text" name="description" id="txtdescription" value={this.state.postdescription} onChange={(e)=>{this.setState({postdescription:e.target.value})}} placeholder="Description" />
                    </FormGroup>
                </Form>
              </ModalBody>
             <ModalFooter>
                <Button color="primary" onClick={this.addPosition}>Submit</Button>{' '}
                <Button color="secondary" onClick={this.toggle}>Cancel</Button>
            </ModalFooter>
            </Modal>
          </div>
        );
      }
  }

class FormUpdatePositionModule extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
          modal: false,
          nestedModal: false,
          backdrop: false,
          postitle:"",
          postdescription:"",
        };
    
        this.toggle = this.toggle.bind(this);
        this.toggleNested = this.toggleNested.bind(this);
        this.updatePosition=this.updatePosition.bind(this);    
      }
    
      toggle() {
        this.setState({
          modal: !this.state.modal
        });
      }
    
      toggleNested() {
        this.setState({
          nestedModal: !this.state.nestedModal
        });
      }

      updatePosition(){
        fetch(this.props.data._links.self.href,{
            method: 'PUT',
            headers: {"Content-Type": "application/json"},
            body: JSON.stringify({
                title:this.state.postitle,
                description: this.state.postdescription,
            })
          }).then((response)=> response.json())
          .then((data)=>console.log(data))
          .catch((err)=> 
            console.log('Request failure: ', err)
          );
    } 
      render() {
        return (
          <div>
            <Button color="primary" size="sm" className="Body-Button" onClick={this.toggle}>Update</Button>{' '}
    
            <Modal isOpen={this.state.modal} toggle={this.toggle} className={this.props.className} className= "modal-lg" backdrop={this.props.backdrop} > 
              <ModalHeader toggle={this.toggle}>Add Position</ModalHeader>
              <ModalBody>
              <Form>
                    <FormGroup>
                    <Label for="exampleText">Title</Label>
                    <Input type="text" name="title" id="txttitle" defaultValue={this.props.data.title} onChange={(e)=>{this.setState({postitle:e.target.value})}} placeholder="Title" />
                    </FormGroup>
                    <FormGroup>
                    <Label for="exampleText">Description</Label>
                    <Input type="text" name="description" id="txtdescription" defaultValue={this.props.data.description} onChange={(e)=>{this.setState({postdescription:e.target.value})}} placeholder="Description" />
                    </FormGroup>
                </Form>
              </ModalBody>
             <ModalFooter>
                <Button color="primary" onClick={this.updatePosition} >Submit</Button>{' '}
                <Button color="secondary" onClick={this.toggle}>Cancel</Button>
            </ModalFooter>
            </Modal>
          </div>
        );
      }
}

export default Positions;