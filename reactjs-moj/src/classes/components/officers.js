import React,{Component} from "react"
import { Container, Row, Col ,Table} from 'reactstrap';
import "./officers.css";
import { Button } from 'reactstrap';
import { Pagination, PaginationItem, PaginationLink } from 'reactstrap';
import {NavLink} from "react-router-dom";
import {Modal, ModalHeader, ModalBody, ModalFooter,Input,Label} from 'reactstrap'
import swal from 'sweetalert'
class Officers extends Component{

    constructor()
    {
        super();
        this.state=
        {

            officers:[]
            ,
            size:10,
            page:0,
            paging:'',
            filter:'enName',
            postion:''
        }
    }
    buildLink(size,page,filter)
    {
        let link= "http://110.74.194.126:16020/api/judicial/officers?page="+page+"&size="+size+"&sort="+filter;
        return link;
    }
    componentWillMount()
    {
        this.loadOfficers(this.buildLink(this.state.size,this.state.page,this.state.filter));
    }
    loadOfficers(url)
    {
        console.log(url);
        fetch(url)
        .then(function(response) {
        if (response.status >= 400) {
        throw new Error("Bad response from server");
        }
        return response.json();
        })
        .then((data)=>{
            console.log(data._embedded.officers);
                this.setState(
                {
                    officers:data._embedded.officers,
                    paging:data.page
                }
            );
        });        
    }
    changePage(topage)
    {
        this.setState((prevState)=>{
            return {
                page:topage,
                officers:[]
            }
        })
        this.loadOfficers(this.buildLink(this.state.size,topage,this.state.filter));
    }
    loadPaging()
    {
        let pageItems=[];
        for(let i=1;i<=this.state.paging.totalPages;i++)
        {
            pageItems.push(
            <PaginationItem>
                <PaginationLink>
                <div onClick={()=>this.changePage(i-1)}>{i}</div>
                </PaginationLink>
            </PaginationItem>
              );
        }
        return pageItems;
    }
    changeSize(e)
    {
        let val= e.target.value;
        this.setState((prevState)=>{
            return {
                size:val,
                officers:[]
            }
        });
        this.loadOfficers(this.buildLink(val,this.state.page,this.state.filter));
    }
    changeFilter(e)
    {
        
        let val= e.target.value;
        this.setState((prevState)=>{
            return {
                filter:val,
                officers:[]
            }
        });
        this.loadOfficers(this.buildLink(this.state.size,this.state.page,val));
        
    }
    render()
    {
        return(
            <div>
                <Row className="filtering">
                    <Col>
                    <Label>ចំនួន ជួរដេក</Label>
                        <select value={this.state.size}  onChange={(e)=>{this.changeSize(e)}}>
                            <option value='5'>5</option>
                            <option value='10'>10</option>
                        </select>
                    </Col>
                    <Col>
                    <Label>តំរៀបតាម</Label>
                        <select value={this.state.filter} onChange={(e)=>{this.changeFilter(e)}}>
                            <option value='khName'>ឈ្មោះជាភាសាខ្មែរ</option>
                            <option value='enName'>ឈ្មោះជាភាសាអង់គ្លេស</option>
                            <option value='currentStatus'>ងារបច្ចុប្បន្ន</option>
                            <option value='dateOfBirth'>អាយុ</option>
                        </select>
                    </Col>
                    <Col>
                        <OfficerAddForm />
                    </Col>
                </Row>
                {this.state.officers.length==0?'No Data Found!':
                this.state.officers.map((currentOfficer)=>{
                    return (
                        <div >
                            <RenderOfficer officer={currentOfficer}/>
                        </div>
                    )
                }
               
                )
                }
                <Row className="pagination">
                    <Col>
                    Current page {this.state.paging.number+1}/{this.state.paging.totalPages}
                    </Col>
                    <Col >
                        <Pagination>
                            <PaginationItem>
                                <PaginationLink>
                                <div onClick={()=>this.changePage(0)}>First</div>
                                </PaginationLink>
                            </PaginationItem>
                            
                            {
                                this.loadPaging()
                            }

                            <PaginationItem>
                                <PaginationLink>
                                    <div onClick={()=>this.changePage(this.state.paging.totalPages-1)}>Last</div>
                                </PaginationLink>
                            </PaginationItem>
                        </Pagination>
                    </Col>
                </Row>
            </div>
        );
    }

}

export class OfficersPositions extends Component{
    
        constructor()
        {
            super();
            this.state=
            {
    
                officers:''
                ,
                positions:[{_links:{
                    officers:{}
                }}],
                saveLink:'http://110.74.194.126:16020/api/judicial/positions/1300/officers'
            }
        }

        loadOfficers(url)
        {
            fetch(url)
            .then(function(response) {
            if (response.status >= 400) {
            throw new Error("Bad response from server");
            }
            return response.json();
            })
            .then((data)=>{
                console.log(data._embedded.officers);
                    this.setState(
                    {
                        officers:data._embedded.officers,
                        paging:data.page
                    }
                );
            });        
        }
        loadPositions()
        {
            let url='http://110.74.194.126:16020/api/judicial/positions';
            fetch(url)
            .then(function(response) {
            if (response.status >= 400) {
            throw new Error("Bad response from server");
            }
            return response.json();
            })
            .then((data)=>{
                    this.setState(
                    {
                        positions:data._embedded.positions
                    }
                );
            });        
        }
        componentWillMount()
        {
            this.loadPositions();
            this.loadOfficers(this.state.saveLink);
        }
        changeSaveLink(e)
        {
            var val =e.target.value;
            this.setState(()=>
            {
                return {
                    saveLink:val,
                    officers:[]
                }
            });
            this.loadOfficers(val);
        }
        
        
        render()
        {
            return(
                <div>
                    <Row className="filtering">
                        <Col>
                        <Label>តំណែង</Label>
                                    <select value={this.state.saveLink} onChange={(e)=>this.changeSaveLink(e)}>
                                       {this.state.positions.map((pos)=>{
                                           return(
                                            <option value={pos._links.officers.href}>
                                                {pos.title}
                                            </option>
                                           )
                                       })}
                                    </select>
                        </Col>
                        <Col>
                            <OfficerAddForm />
                        </Col>
                    </Row>
                    {this.state.officers.length==0?'No Data Found!':
                    this.state.officers.map((currentOfficer)=>{
                        return (
                            <div >
                                <RenderOfficer officer={currentOfficer}/>
                            </div>
                        )
                    }
                   
                    )
                    }
                </div>
            );
        }
    
    }

export class OfficerAddForm extends Component
{

    constructor(props) {
        super(props);
        this.state = {
          modal: false,

            positions:[{_links:{
                officers:{}
            }}],
            file:'',
            khName:'',
            enName:'',
            dateOfBirth:'',
            placeOfBirth:'',
            degree:'',
            other:'',
            office:'',
            image:'',
            currentStatus:'',
            phoneNumber:[],
            fax:[],
            website:[],
            saveLink:''
        };
    
        this.toggle = this.toggle.bind(this);
      }
    
      toggle() {
        this.setState({
          modal: !this.state.modal
        });
      }

    changeKhName(e)
    {
        let val=e.target.value;
        this.setState(()=>
        {
            return {
                khName:val
            }
        });
    }
    changeEnName(e)
    {
        let val=e.target.value;
        this.setState(()=>
        {
            return {
                enName:val
            }
        });
    }
    changeOffice(e)
    {
        let val=e.target.value;
        this.setState(()=>
        {
            return {
                office:val
            }
        });
    }
    changeDegree(e)
    {
        let val=e.target.value;
        this.setState(()=>
        {
            return {
                degree:val
            }
        });
    }
    changeCurrentStatus(e)
    {
        let val=e.target.value;
        this.setState(()=>
        {
            return {
                currentStatus:val
            }
        });
    }
    changeDOB(e)
    {
        let val=e.target.value;
        this.setState(()=>
        {
            return {
                dateOfBirth:val
            }
        });
    }
    changePOB(e)
    {
        let val=e.target.value;
        this.setState(()=>
        {
            return {
                placeOfBirth:val
            }
        });
    }
    changeOther(e)
    {
        let val=e.target.value;
        this.setState(()=>
        {
            return {
                other:val
            }
        });
    }
    changeFax(e)
    {
        let val=e.target.value;
        this.setState(()=>
        {
            return {
                fax:[val]
            }
        });
    }
    changePhone(e)
    {
        let val=e.target.value;
        this.setState(()=>
        {
            return {
                phoneNumber:[val]
            }
        });
    }
    changeWeb(e)
    {
        let val=e.target.value;
        this.setState(()=>
        {
            return {
                website:[val]
            }
        });
    }
    changeFile(e)
    {
        let formData=new FormData();
        console.log("file selected");
        let files = e.target.files[0];
        formData.append("files",files);
        this.setState(()=>{
            return{file:formData}
        });
        this.uploadFile(formData);
    }   
    uploadFile(formData)
    {
        console.log("Setting state");
        console.log("uploadin image");
        fetch('http://110.74.194.126:16010/pr/uploads', {
            method: 'POST',
            contentType: false,
            processData: false,
            body: formData
          }).then((res)=>res.json()).then(data =>{
            console.log(data)
            this.setState({
                image:"http://110.74.194.126:16010"+data[0]
            });
          } );
          
    }
    loadPositions()
    {
        let url='http://110.74.194.126:16020/api/judicial/positions';
        fetch(url)
        .then(function(response) {
        if (response.status >= 400) {
        throw new Error("Bad response from server");
        }
        return response.json();
        })
        .then((data)=>{
                this.setState(
                {
                    positions:data._embedded.positions
                }
            );
        });        
    }
    changeImage(e)
    {
        let val=e.target.value;
        this.setState(()=>
        {
            return {
                image:val
            }
        });
    }
    changeSaveLink(e)
    {
        var val =e.target.value;
        this.setState(()=>
        {
            return {
                saveLink:val
            }
        });
    }
    saveOfficer()
    {
        
        swal({
            title: "តើប្រាកដទេ?",
            text: "តើអ្នកប្រាកដថាចង់រក្សាទុកព័ត៌មានមែនឬ?",
            icon: "warning",
            buttons: true,
            dangerMode: true,
          })
          .then(willSave => {
            if (willSave) {
                let data=
                {
                    
                    khName:this.state.khName,
                    enName:this.state.enName,
                    dateOfBirth:this.state.dateOfBirth,
                    placeOfBirth:this.state.placeOfBirth,
                    degree:this.state.degree,
                    office:this.state.office,
                    other:this.state.other,
                    currentStatus:this.state.currentStatus,
                    phoneNumber:this.state.phoneNumber,
                    fax:this.state.fax,
                    website:this.state.website,
                    image:this.state.image
                }
                fetch(this.state.saveLink,{
                    method:'POST',
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json'
                    },
                    body:JSON.stringify(data)
                })
                .then(function(response)
                {
                    swal("បានរក្សាទុករួចរាល់!", "ទិន្នន័យមន្រ្តី យុត្តិធម៌ត្រូវបានរក្សាទុករួចរាល់", "success");
                    // window.location.reload();
                }
                );
                
                
            }
            else
            {
                swal("មិនបានរក្សាទុក!", "ទិន្នន័យមន្រ្តី យុត្តិធម៌មិនត្រូវបានរក្សាទុក", "warning");
            }
            this.toggle();
          });

          
    }
    componentWillMount()
    {
        this.loadPositions();
    }
    render()
    {

        return(
        <div>
            <Button color="primary" onClick={this.toggle}>បន្ថែមព័ត៌មានមន្ត្រីយុត្តិធ៌ម</Button>
            <Modal isOpen={this.state.modal} toggle={this.toggle} size="lg" className="modalform">
                <ModalHeader toggle={this.toggle}>បន្ថែមព័ត៌មានមន្ត្រីយុត្តិធ៌ម</ModalHeader>
                <ModalBody>
                    <Table>
                        <tbody>
                            
                            <tr>
                                <td><Label>ឈ្មោះជាភាសាខ្មែរ</Label></td>
                                <td><Input type="text" value={this.state.khName} onChange={(e)=>this.changeKhName(e)}/></td>
                                <td><Label>ឈ្មោះជាភាសាអង់គ្លេស</Label></td>
                                <td><Input type="text" value={this.state.enName} onChange={(e)=>this.changeEnName(e)}/></td>
                            </tr>
                            <tr>
                                <td><Label>ថ្ងៃខែឆ្នាំកំណើត</Label></td>
                                <td><Input type="date"  value={this.state.dateOfBirth} onChange={(e)=>this.changeDOB(e)}/></td>
                                <td><Label>ទីកន្លែងកំណើត</Label></td>
                                <td><Input type="text" value={this.state.placeOfBirth} onChange={(e)=>this.changePOB(e)}/></td>
                            </tr>
                            <tr>
                                <td><Label>កម្រិតអប់រំ</Label></td>
                                <td><Input type="text" value={this.state.degree} onChange={(e)=>this.changeDegree(e)}/></td>
                                <td><Label>ទីស្នាក់ការ</Label></td>
                                <td><Input type="text" value={this.state.office} onChange={(e)=>this.changeOffice(e)}/></td>
                            </tr>
                            <tr>
                                <td><Label>ព័ត៌មានផ្សេង</Label></td>
                                <td><Input type="textarea" value={this.state.other} onChange={(e)=>this.changeOther(e)}/></td>
                                <td><Label>Website</Label></td>
                                <td>
                                    {<Input type="text" value={this.state.website==null?null:this.state.website[0]} onChange={(e)=>this.changeWeb(e)}/>}
                                </td>
                            </tr>
                            <tr>
                                <td><Label>ទំនាក់ទំនង(Tel)</Label></td>
                                {<td><Input type="text" value={this.state.phoneNumber==null?null:this.state.phoneNumber[0]} onChange={(e)=>this.changePhone(e)}/></td>}
                                <td><Label>ទំនាក់ទំនង(Fax)</Label></td>
                                {<td><Input type="text" value={this.state.fax==null?null:this.state.fax[0]} onChange={(e)=>this.changeFax(e)}/></td>}
                            </tr>
                            <tr>
                                <td><Label>អស័យដ្ឋាន រូបភាព</Label></td>
                                <td><Input type="text" value={this.state.image} onChange={(e)=>this.changeImage(e)}/></td>
                                <td><Label>មុខតំណែង</Label></td>
                                <td>
                                    <Label>{this.state.position}</Label>
                                    <select value={this.state.saveLink} onChange={(e)=>this.changeSaveLink(e)}>
                                       {this.state.positions.map((pos)=>{
                                           return(
                                            <option value={pos._links.officers.href}>
                                                {pos.title}
                                            </option>
                                           )
                                       })}
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td><Label>ដាក់រូបភាព</Label></td>
                                <td><Input type="file" name="file" id="exampleFile" onChange={(e)=>this.changeFile(e)} /></td>
                                <td ><Label>ងារបច្ចុប្បន្ន</Label></td>
                                <td ><Input type="text" value={this.state.currentStatus} onChange={(e)=>this.changeCurrentStatus(e)}/></td>
                            </tr>
                        </tbody>
                    </Table>
                </ModalBody>
                <ModalFooter>
                    <Button color="primary"onClick={(e)=>{this.saveOfficer()}}>បន្ថែមព័ត៌មាន</Button>{' '}
                    
                    <Button color="secondary" onClick={this.toggle}>បោះបង់</Button>
                </ModalFooter>
            </Modal>
        </div>
        )

    }

}


// ======================================================================
export class RenderOfficer extends Component
{
    constructor(props) {
        super(props);
        this.state = {
          modal: false,
          isMore:false,
          notRemove:true,
          position:'',
            file:'',
            positions:[{_links:{
                officers:{}
            }}],
            
            saveLink:'',
            khName:this.props.officer.khName,
            enName:this.props.officer.enName,
            dateOfBirth:this.props.officer.dateOfBirth,
            placeOfBirth:this.props.officer.placeOfBirth,
            degree:this.props.officer.degree,
            other:this.props.officer.other,
            office:this.props.officer.office,
            image:this.props.officer.image,
            currentStatus:this.props.officer.currentStatus,
            phoneNumber:this.props.officer.phoneNumber,
            fax:this.props.officer.fax,
            website:this.props.officer.website,
            _links:this.props.officer._links
        };
    
        this.toggle = this.toggle.bind(this);
      }
    
      toggle() {
        this.setState({
          modal: !this.state.modal
        });
      }
    loadPosition(url)
    {

        console.log("link to position",this.props.officer._links.position.href);
        fetch(url)
        .then(function(response) {
        if (response.status >= 400) {
        throw new Error("Bad response from server");
        }
        return response.json();
        })
        .then((data)=>{
                this.setState(
                {
                    position:data,
                    saveLink:data._links.officers.href
                }
            );
        });
    }
    changeKhName(e)
    {
        let val=e.target.value;
        this.setState(()=>
        {
            return {
                khName:val
            }
        });
    }
    changeEnName(e)
    {
        let val=e.target.value;
        this.setState(()=>
        {
            return {
                enName:val
            }
        });
    }
    changeOffice(e)
    {
        let val=e.target.value;
        this.setState(()=>
        {
            return {
                office:val
            }
        });
    }
    changeDegree(e)
    {
        let val=e.target.value;
        this.setState(()=>
        {
            return {
                degree:val
            }
        });
    }
    changeCurrentStatus(e)
    {
        let val=e.target.value;
        this.setState(()=>
        {
            return {
                currentStatus:val
            }
        });
    }
    changeDOB(e)
    {
        let val=e.target.value;
        console.log(val);
        this.setState(()=>
        {
            return {
                dateOfBirth:val
            }
        });
    }
    changePOB(e)
    {
        let val=e.target.value;
        this.setState(()=>
        {
            return {
                placeOfBirth:val
            }
        });
    }
    changeOther(e)
    {
        let val=e.target.value;
        this.setState(()=>
        {
            return {
                other:val
            }
        });
    }
    changeFax(e)
    {
        let val=e.target.value;
        this.setState(()=>
        {
            return {
                fax:[val]
            }
        });
    }
    changePhone(e)
    {
        let val=e.target.value;
        this.setState(()=>
        {
            return {
                phoneNumber:[val]
            }
        });
    }
    changeWeb(e)
    {
        let val=e.target.value;
        this.setState(()=>
        {
            return {
                website:[val]
            }
        });
    }
    changeImage(e)
    {
        let val=e.target.value;
        this.setState(()=>
        {
            return {
                image:val
            }
        });
    }
    changeFile(e)
    {
        let formData=new FormData();
        console.log("file selected");
        let files = e.target.files[0];
        formData.append("files",files);
        this.setState(()=>{
            return{file:formData}
        });
        this.uploadFile(formData);
    }   
    uploadFile(formData)
    {
        console.log("Setting state");
        console.log("uploadin image");
        fetch('http://110.74.194.126:16010/pr/uploads', {
            method: 'POST',
            contentType: false,
            processData: false,
            body: formData
          }).then((res)=>res.json()).then(data =>{
            console.log(data)
            this.setState({
                image:"http://110.74.194.126:16010"+data[0]
            });
          } );
          
    }
    updateOfficer()
    {
        var currentId=this.props.officer._links.self.href;
        currentId=currentId.substring("http://110.74.194.126:16020/api/judicial/positions/".length-1,currentId.length);
        console.log(currentId);
        let data=
        {
            id:currentId,
            khName:this.state.khName,
            enName:this.state.enName,
            dateOfBirth:this.state.dateOfBirth,
            placeOfBirth:this.state.placeOfBirth,
            degree:this.state.degree,
            office:this.state.office,
            other:this.state.other,
            currentStatus:this.state.currentStatus,
            phoneNumber:this.state.phoneNumber,
            fax:this.state.fax,
            website:this.state.website,
            image:this.state.image
        }
        let url=this.state.saveLink;
        console.log("Update url",url);
        swal({
            title: "តើប្រាកដទេ?",
            text: "តើអ្នកប្រាកដថាចង់កែប្រែព័ត៌មានមែនឬ?",
            icon: "warning",
            buttons: true,
            dangerMode: true,
          })
          .then(willUpdate => {
            if (willUpdate) {
            
                fetch(url,{
                    method:'POST',
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json'
                    },
                    body:JSON.stringify(data)
                })
                .then(function(response)
                {
                    swal("បានកែប្រែរួចរាល់!", "ទិន្នន័យមន្រ្តី យុត្តិធម៌ត្រូវបានកែប្រែរួចរាល់", "success");
                }
                );
                let l=this.state.saveLink;
                l=l.substring(0,l.length-9);
                console.log(l);
                this.loadPosition(l);
            }
            else
            {
                swal("មិនបានកែប្រែ!", "ទិន្នន័យមន្រ្តី យុត្តិធម៌មិនត្រូវបានកែប្រែ", "warning");
            }
            this.toggle();
          });


        
    }
    deleteOfficer()
    {
        let url=this.props.officer._links.self.href;
        console.log("Delete Url ",url);
        swal({
            title: "តើប្រាកដទេ?",
            text: "តើអ្នកប្រាកដថាចង់កែប្រែព័ត៌មានមែនឬ?",
            icon: "warning",
            buttons: true,
            dangerMode: true,
          })
          .then(willDelete => {
            if (willDelete) {
            
                fetch(url,{
                    method:'DELETE',
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json'
                    }
                })
                .then(function(response)
                {
                    swal("បានលុបរួចរាល់!", "ទិន្នន័យមន្រ្តី យុត្តិធម៌ត្រូវបានលុបរួចរាល់", "success");
                }
                ); 

                this.hideOfficer();
            }
            else
            {
                swal("មិនបានលុប!", "ទិន្នន័យមន្រ្តី យុត្តិធម៌មិនត្រូវបានលុប", "warning");
            }
          });

    }
    hideOfficer()
    {
        this.setState(()=>
        {
            return {notRemove:false}        
        });
    }
    showToggle()
    {
        this.setState((prevState)=>
    {
        return {
            isMore:!this.state.isMore
        }
    })
    }
    componentWillMount()
    {
        this.setState(()=>{
            return {
                khName:this.props.officer.khName,
                enName:this.props.officer.enName,
                dateOfBirth:this.props.officer.dateOfBirth,
                placeOfBirth:this.props.officer.placeOfBirth,
                degree:this.props.officer.degree,
                office:this.props.officer.office,
                image:this.props.officer.image,
                currentStatus:this.props.officer.currentStatus,
                phoneNumber:this.props.officer.phoneNumber,
                fax:this.props.officer.fax,
                website:this.props.officer.website,
                _links:this.props.officer._links
            }
        }
    );
    this.loadPositions();
    }
    loadPositions()
    {
        this.loadPosition(this.props.officer._links.position.href);
        let url='http://110.74.194.126:16020/api/judicial/positions';
        fetch(url)
        .then(function(response) {
        if (response.status >= 400) {
        throw new Error("Bad response from server");
        }
        return response.json();
        })
        .then((data)=>{
                this.setState(
                {
                    positions:data._embedded.positions
                }
            );
        });      
    }
    changeSaveLink(e)
    {
        var val =e.target.value;
        this.setState(()=>
        {
            return {
                saveLink:val
            }
        });
    }
    timeConverter(ISO_8601){
        var date = new Date(ISO_8601);
        return date.toLocaleDateString();

        }
    render()
    {
        return(
            
            <div>
                {this.state.notRemove?
                 <div>
                     <Row className="row-officer">
                        <Col lg="12" md="12" sm="12" xs="12" className="title-wrapper">
                            <h4 className="title">{this.state.currentStatus}</h4>
                        </Col>
                        <Col lg="4" md="4" sm="12" xs="12">
                            <img src={this.state.image} className="profile"/>
                        </Col>
                        <Col lg="8" md="8" sm="12" xs="12" className="details-wrapper">
                            <Table>
                                <tbody className="row-details">
                                    <tr>
                                        <td>ឈ្មោះ</td>
                                        <td>{this.state.khName}</td>
                                    </tr>
                                    <tr>
                                        <td>Name</td>
                                        <td>{this.state.enName}</td>
                                    </tr>
                                    <tr>
                                        <td>ថ្ងៃខែឆ្នាំកំណើត</td>
                                        <td>{this.timeConverter(this.state.dateOfBirth)}</td>
                                    </tr>
                                    <tr>
                                        <td>ទីកន្លែងកំណើត</td>
                                        <td>{this.state.placeOfBirth}</td>
                                    </tr>
                                    <tr>
                                        <td>កម្រិតអប់រំ</td>
                                        <td>{this.state.degree}</td>
                                    </tr>
                                    <tr>
                                        <td>ទីស្នាក់ការ</td>
                                        <td>{this.state.office}
                                        </td>
                                        
                                    </tr>
                                    <tr>
                                        <td><Label>មុខតំណែង</Label></td>
                                        <td>
                                            <Label>{this.state.position==null?null:this.state.position.title}</Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><Button onClick={()=>{this.showToggle()}}>{this.state.isMore?'បន្ថយព័ត៌មាន':'ព័ត៌មានបន្ថែម'}</Button></td>
                                        <td>
                                            <Row>
                                            <Col>

                                            <div>
                                                <Button color="warning" onClick={this.toggle}>កែប្រែ</Button>
                                                <Modal isOpen={this.state.modal} toggle={this.toggle} size="lg" className="modalform">
                                                    <ModalHeader toggle={this.toggle}>កែប្រែ មន្ត្រី យុត្តិធ៌ម</ModalHeader>
                                                    <ModalBody>
                                                        <Table>
                                                            <tbody>
                                                                
                                                                <tr>
                                                                    <td><Label>ឈ្មោះជាភាសាខ្មែរ</Label></td>
                                                                    <td><Input type="text" value={this.state.khName} onChange={(e)=>this.changeKhName(e)}/></td>
                                                                    <td><Label>ឈ្មោះជាភាសាអង់គ្លេស</Label></td>
                                                                    <td><Input type="text" value={this.state.enName} onChange={(e)=>this.changeEnName(e)}/></td>
                                                                </tr>
                                                                <tr>
                                                                    <td><Label>ថ្ងៃខែឆ្នាំកំណើត</Label></td>
                                                                    <td><Input type="date"  value={this.state.dateOfBirth} onChange={(e)=>this.changeDOB(e)}/></td>
                                                                    <td><Label>ទីកន្លែងកំណើត</Label></td>
                                                                    <td><Input type="text" value={this.state.placeOfBirth} onChange={(e)=>this.changePOB(e)}/></td>
                                                                </tr>
                                                                <tr>
                                                                    <td><Label>កម្រិតអប់រំ</Label></td>
                                                                    <td><Input type="text" value={this.state.degree} onChange={(e)=>this.changeDegree(e)}/></td>
                                                                    <td><Label>ទីស្នាក់ការ</Label></td>
                                                                    <td><Input type="text" value={this.state.office} onChange={(e)=>this.changeOffice(e)}/></td>
                                                                </tr>
                                                                <tr>
                                                                    <td><Label>ព័ត៌មានផ្សេង</Label></td>
                                                                    <td><Input type="textarea" value={this.state.other} onChange={(e)=>this.changeOther(e)}/></td>
                                                                    <td><Label>Website</Label></td>
                                                                    <td>
                                                                        {<Input type="text" value={this.state.website==null?null:this.state.website[0]} onChange={(e)=>this.changeWeb(e)}/>}
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td><Label>ទំនាក់ទំនង(Tel)</Label></td>
                                                                    {<td><Input type="text" value={this.state.phoneNumber==null?null:this.state.phoneNumber[0]} onChange={(e)=>this.changePhone(e)}/></td>}
                                                                    <td><Label>ទំនាក់ទំនង(Fax)</Label></td>
                                                                    {<td><Input type="text" value={this.state.fax==null?null:this.state.fax[0]} onChange={(e)=>this.changeFax(e)}/></td>}
                                                                </tr>
                                                                <tr>
                                                                    <td><Label>អស័យដ្ឋាន រូបភាព</Label></td>
                                                                    <td><Input type="text" value={this.state.image} onChange={(e)=>this.changeImage(e)}/></td>
                                                                    <td><Label>មុខតំណែង</Label></td>
                                                                    <td>
                                                                        <select value={this.state.saveLink} onChange={(e)=>this.changeSaveLink(e)}>
                                                                        {this.state.positions.map((pos)=>{
                                                                            return(
                                                                                <option value={pos._links.officers.href}>
                                                                                    {pos.title}
                                                                                </option>
                                                                            )
                                                                        })}
                                                                        </select>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td><Label>ដាក់រូបភាព</Label></td>
                                                                    <td><Input type="file" name="file" id="exampleFile" onChange={(e)=>this.changeFile(e)} /></td>
                                                                    <td ><Label>ងារបច្ចុប្បន្ន</Label></td>
                                                                    <td ><Input type="text" value={this.state.currentStatus} onChange={(e)=>this.changeCurrentStatus(e)}/></td>
                                                                </tr>
                                                            </tbody>
                                                        </Table>
                                                    </ModalBody>
                                                    <ModalFooter>
                                                        <Button color="primary"onClick={(e)=>{this.updateOfficer()}}>កែប្រែ</Button>{' '}
                                                        <Button color="secondary" onClick={this.toggle}>បោះបង់</Button>
                                                    </ModalFooter>
                                                </Modal>
                                            </div>
                                            
                                            </Col>
                                        
                                            <Col><Button color='danger' onClick={()=>this.deleteOfficer()}>លុប</Button></Col>
                                            </Row>
                                        </td>
                                        
                                    </tr>
                                    
                                    
                                </tbody>
                                {
                                        this.state.isMore?
                                        <tbody className="row-details">
                                            <tr>
                                                <td>ព័ត៌មានផ្សេង</td>
                                                <td>{this.state.other}</td>
                                            </tr>
                                            <tr>
                                                <td>ទំនាក់ទំនង(Tel)</td>
                                                <td>{this.state.phoneNumber.map((current)=>
                                                <div key={current}>
                                                    <div>:{current}</div>
                                                </div>
                                                )}</td>
                                                
                                            </tr>
                                            <tr>
                                                <td>ទំនាក់ទំនង(Fax)</td>
                                                <td>{this.state.fax.map((current)=>
                                                <div key={current}>
                                                    <div>:{current}</div>
                                                </div>
                                                )}</td>
                                                
                                            </tr>
                                            <tr>
                                                <td>Website</td>
                                                <td>{this.state.website.map((current)=>
                                                <div key={current}>
                                                    <div>:{current}</div>
                                                </div>
                                                )}</td>
                                                
                                            </tr>
                                        </tbody>
                                        :null
                                    }
                            </Table>
                        </Col>
                    </Row>

                </div>
                :null}
                    
            </div>
        );
    }
}

export default Officers;
