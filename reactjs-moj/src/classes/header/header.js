import React,{Component} from "react";
import { Container, Row, Col } from 'reactstrap';
import { Collapse, Navbar, NavbarToggler, NavbarBrand, Nav, NavItem ,NavLink} from 'reactstrap';
import './header.css'
import logo from "./MOJ.png"

class Header extends Component
{
    constructor(props) {
        super(props);

        this.toggleNavbar = this.toggleNavbar.bind(this);
        this.state = {
        collapsed: true
        };
    }

  toggleNavbar() {
    this.setState({
      collapsed: !this.state.collapsed
    });
  }


    render()
    {
        return (
            <div>
                <Container className="header-wrapper">
                    <Row>
                        <Col xs="12" sm="4">
                            <NavLink exact to="/"><img src={logo} className="logo" /></NavLink>
                        </Col>
                        <Col xs="12" sm="8" className="caption">
                            <h1>ព្រះរាជាណាចក្រ កម្ពុជា</h1>
                            <h3>ជាតិ សាសនា ព្រះមហាក្សត្រ</h3>
                            <h2>ក្រសួង យុត្ដិធ៌ម</h2>
                        </Col>
                    </Row>
                    <Row  className="navigation">
                        <Col>
                            <Navbar color="faded" light>
                                <NavbarBrand href="/" className="mr-auto">ទំព័រដើម</NavbarBrand>
                                <NavbarToggler onClick={this.toggleNavbar} className="mr-2" />
                                <Collapse isOpen={!this.state.collapsed} navbar>
                                    <Nav navbar>
                                    <NavItem>
                                        <NavLink href="/officers">មន្រ្តីយុត្តិធម៌</NavLink>
                                    </NavItem>
                                    <NavItem>
                                        <NavLink href="/positions">មុខតំណែង</NavLink>
                                    </NavItem>
                                    <NavItem>
                                        <NavLink href="/officerspositions">មន្រ្តីតាមមុខតំណែង</NavLink>
                                    </NavItem>
                                    </Nav>
                                </Collapse>
                            </Navbar>
                        </Col>
                    </Row>
                </Container>
                
            </div>
        );
    }

}



export default Header;
