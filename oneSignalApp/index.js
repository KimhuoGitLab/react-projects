import { AppRegistry } from 'react-native';
import App from './OneSignalApp';

AppRegistry.registerComponent('oneSignalApp', () => App);
