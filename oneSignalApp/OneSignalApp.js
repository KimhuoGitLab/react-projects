import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,

} from 'react-native';

import OneSignal from 'react-native-onesignal';

export default class App extends Component<{}> {

  componentDidMount() {
    OneSignal.configure({});
    // OneSignal.addEventListener('received', this.onReceived);
    OneSignal.addEventListener('opened', this.onOpened);
  }

  onOpened(openResult) {
    console.log('Data: ', openResult.notification.payload.additionalData);
    console.log('Message: ', openResult.notification.payload.body);
    console.log('Title: ', openResult.notification.payload.title);
  }

  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.welcome}>
          Welcome to React Native - OneSignal App!
        </Text>
        
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});
