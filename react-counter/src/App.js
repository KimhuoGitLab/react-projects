import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

class App extends Component {

  constructor(){
    super();

    this.state = {
      count: 0,
    }
  }

  Counter(){
    this.setState({
      count: this.state.count + 1
    });
  } 

  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Welcome to React</h1>
        </header>
        <p className="App-intro">
          To get started, edit <code>src/App.js</code> and save to reload.
        </p>

        <button
          onClick={()=>{this.Counter()}}
        >
          Clicked {this.state.count} times
        </button>

      </div>
    );
  }
}

export default App;
