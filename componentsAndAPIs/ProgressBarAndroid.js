import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  ProgressBarAndroid,

} from 'react-native';

export default class App extends Component {

  render() {
    return (
      <ProgressBarAndroid styleAttr='LargeInverse'/>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  text: {
    fontSize: 14,
    textAlign: 'center',
    fontWeight: '500',
    margin: 10,
  },
});
