import React from 'react';
import {
   StyleSheet,
   Text,
   View,
   DatePickerAndroid,
   DatePickerIOS,
   Modal,
   TouchableHighlight,
   Button
  } from 'react-native';

export default class App extends React.Component {
  constructor(){
    super();
    this.state ={selectedValue:""}
  }
  showAndroidDatePicker = async () => {
    try {
        const {action, year, month, day} = await DatePickerAndroid.open({
            date: new Date()
        });
        if (action !== DatePickerAndroid.dismissedAction) {
            var date = new Date(year, month, day);
            this.setState({selectedValue: date.toLocaleDateString()});
        }
    } catch ({code, message}) {
        console.log('Cannot open date picker', message);
        console.log('test');
    }
};


  render() {
    return (
      <View>
        <Button onPress={this.showAndroidDatePicker} title="Show Datepicker"/>
        <Text>{this.state.selectedValue}</Text>
        </View>

    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
