import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  DatePickerAndroid,

} from 'react-native';
import { date } from './C:/Users/KIMHUO/AppData/Local/Microsoft/TypeScript/2.6/node_modules/@types/assert-plus';
import { error } from 'util';

export default class App extends Component {

  constructor(props) {
    super(props);

    this.state = {
      result: 'default'
    }
  }

  componentWillMount(){
    const self = this;
    const today = new Date();
    const theMinDate = new Date(2013, 1, 1);
    const theMaxDate = new Date(2018, 1, 1);
    const option = {
      date: today,
      minDate: theMinDate,
      maxDate: theMaxDate,
    };
  }

  DatePickerAndroid.open(option).then(
    result => {
      if (result.action === DatePickerAndroid.dismissedAction) {
        self.setState(
          {
            result: 'You did not choose a date yet!!',
          }
        );       
      } else {
        self.setState(
          {
            result: 'Year' + result.year + 'Month' + result.month + 'Day' + result.day,
          }
        );
      }
    }
  ).catch (
    error => {
      console.log('Error: ' + error);
    }
  );

  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.welcome}>
          Welcome to React Native!
        </Text>
        <Text style={styles.instructions}>
          To get started, DatePickerAndroid
        </Text>
        
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  text: {
    fontSize: 14,
    textAlign: 'center',
    fontWeight: '500',
    margin: 10,
  },
});
