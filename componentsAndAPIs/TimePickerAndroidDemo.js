import React from 'react';
import {
   StyleSheet,
   Text,
   View,
   DatePickerAndroid,
   DatePickerIOS,
   Modal,
   TouchableHighlight,
   Button,
   TimePickerAndroid,

  } from 'react-native';

export default class App extends React.Component {
  constructor(){
    super();
    this.state ={selectedValue:""}
  }
  showAndroidTimePicker = async () => {
    /* try {
        const {action, year, month, day} = await DatePickerAndroid.open({
            date: new Date()
        });
        if (action !== DatePickerAndroid.dismissedAction) {
            var date = new Date(year, month, day);
            this.setState({selectedValue: date.toLocaleDateString()});
        }
    } catch ({code, message}) {
        console.log('Cannot open date picker', message);
        console.log('test');
    } */

    try {
      const {action, hour, minute} = await TimePickerAndroid.open({
        hour: 14,
        minute: 0,
        is24Hour: false, // Will display '2 PM'

        time: new Date()
      });
      if (action !== TimePickerAndroid.dismissedAction) {
        // Selected hour (0-23), minute (0-59)

        var time = new Date (hour, minute);
        this.setState({selectedValue: time.toLocaleDateString()});
      }
    } catch ({code, message}) { 
      console.warn('Cannot open time picker', message);
    }
};

  render() {
    return (
      <View>
        <Button onPress={this.showAndroidTimePicker} title="Show Timepicker"/>
        <Text>{this.state.selectedValue}</Text>
        </View>

    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
