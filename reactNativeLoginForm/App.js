import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  Image,
  TextInput,
  TouchableOpacity,
  Linking,
  WebView,
  Alert,
  ScrollView,
  Button,
  Picker,
  View,
  Switch,
} from 'react-native';

export default class App extends Component {

  constructor(props){
    super(props);

    this.state={
      selectedGender: '',
    }
  }

  //BtnFunction
  _onPressButton() {
    Alert.alert('You tapped the LOG IN button!')
  }

  render() {

    return (
      <ScrollView contentContainerStyle={styles.container} 
        showsVerticalScrollIndicator={false}>
        <Image
          /* style={{width: 200, height: 200}} */
          style={styles.stretch}
          resizeMode="contain"
          source={require('./img/react-native-logo.png')}
        />

        <Text style={styles.welcome}>
          Welcome to React Native Login Form!
        </Text>

        <Text style={styles.instructions}>
          To get started,
        </Text>

        <Text style={styles.instructions}>
          Please input your Username and Password
        </Text>

        <TextInput
          style={styles.txtInput}        
          underlineColorAndroid= 'rgba(0,0,0,0)'
          placeholder='Username'
          keyboardType='email-address'
        />

        <TextInput
          secureTextEntry={true}
          style={styles.txtInput}
          underlineColorAndroid= 'rgba(0,0,0,0)'
          placeholder='Password'
          keyboardType='numeric'
        />

        <TextInput
          style={styles.txtInput}
          underlineColorAndroid= 'rgba(0,0,0,0)'
          placeholder='Email | Phone Number'
          keyboardType='email-address'
        />

        <View style={styles.picker}>
          <Picker
            selectedValue={this.state.selectedGender}
            onValueChange={(value) => this.setState({selectedGender: value})}
            mode="dropdown">
            <Picker.Item label="Gender" value="gender" />
            <Picker.Item label="Male" value="male" />
            <Picker.Item label="Female" value="female" />
          </Picker>
        </View>

        <View style={styles.switchStyle}>
          <Switch
            onValueChange={(value) => this.setState({falseSwitchIsOn: value})}
            value={this.state.falseSwitchIsOn} />

          <Text style={styles.txtSwitch}>
            I have read and agreed all terms of service and conditions...
          </Text>
        </View>

        <View style={styles.btnLogin}>
          <Button
            color= "grey"
            title= 'REGISTER'
            onPress={this._onPressButton}
          />
        </View>

        <TouchableOpacity>
          <Text
              style={styles.touchOpa}
              onPress={() => {Linking.openURL('http://www.facebook.com/Kimhuo99')}}
          >
              SIGN UP
          </Text>
        </TouchableOpacity>

      </ScrollView>
    );
  }
}

// StyleSheet
const styles = StyleSheet.create({
  container: {
    // flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
    paddingTop: 15,
    paddingBottom: 15,
  },
  welcome: {
    fontSize: 18,
    textAlign: 'center',
    margin: 10,
    fontWeight: 'bold',
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  stretch: {
    width: 150,
    height: 150,
  },
  txtInput: {
    color: 'black',
    width: 260,
    height: 40, 
    borderColor: 'gray', 
    borderWidth: 1,
    margin: 5,
    padding: 10,
    backgroundColor: '#FFFFFF',
  },
  btnLogin: {
    margin: 5,
    width: 260,
  },
  touchOpa: {
    textDecorationLine: 'underline',
    color: '#2096F3',
  },
  picker: {
    width: 260,
    height: 50,
    margin: 5,
    borderColor: 'gray', 
    borderWidth: 1,
    backgroundColor: '#FFFFFF',
  },
  switchStyle: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    width: 220,
  },
  txtSwitch: {
    fontSize: 14,
    color: '#333333',
    paddingLeft: 5,   
  },
});
