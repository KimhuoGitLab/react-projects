import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Picker,
  AppState,

} from 'react-native';

import PushNotification from 'react-native-push-notification';

export default class App extends Component {
  constructor(){
    super();
    this.state = {
      seconds: 5,
    }
  }

  componentDidMount(){
    // var PushNotification = require('react-native-push-notification');

    PushNotification.configure({
    
        // (required) Called when a remote or local notification is opened or received
        onNotification: function(notification) {
            console.log( 'NOTIFICATION:', notification );
    
            // process the notification
        },
    
        // Should the initial notification be popped automatically
        // default: true
        popInitialNotification: true,
    
        /**
          * (optional) default: true
          * - Specified if permissions (ios) and token (android and ios) will requested or not,
          * - if not, you must call PushNotificationsHandler.requestPermissions() later
          */
        requestPermissions: true,
    })

    AppState.addEventListener('change', this.handleAppStateChange.bind(this));
  }

  componentWillUnmount(){
    AppState.removeEventListener('change', this.handleAppStateChange.bind(this));
  }

  handleAppStateChange(appState){
    if(appState==='background'){
      console.log('App is in Background and you set to alert in: ', this.state.seconds);
      let date = new Date(Date.now() + (this.state.seconds*1000));
      console.log(date);
      PushNotification.localNotificationSchedule({
        message: 'You set ' + this.state.seconds + ' seconds to alert!',
        date: new Date(Date.now() + (this.state.seconds*1000))
      });
    }
  }

  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.welcome}>
          Welcome to React Native Push Notification in Local!
        </Text>
        <Text style={styles.instructions}>
          Please choose value to set alert:
        </Text>

        <Picker
          style={styles.pickerStyle}
          selectedValue={this.state.seconds}
          onValueChange={(seconds) => this.setState({seconds})}
          >
          <Picker.Item label="5" value={5}/>
          <Picker.Item label="10" value={10}/>
          <Picker.Item label="15" value={15}/>
          
        </Picker>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  pickerStyle: {
    width: 100,
  },
});
