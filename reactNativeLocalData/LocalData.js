import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  AsyncStorage,
  Button,

} from 'react-native';

export default class LocalData extends Component {

  saveData() {
    let user = 'Kimhuo'
    AsyncStorage.setItem('user', user);
    alert('You clicked to save data into local data!');
  }
  
  displayData = async () => {
    try {
      let user = await AsyncStorage.getItem('user');
      alert(user);
  
    } catch(error) {
      alert(error);
    }
  }

  render() {
    return (
      <View style={styles.container}>
       <Button 
        onPress={this.saveData.bind(this)}
        title='Click here to save Data!'>
       </Button>

       <Text></Text>

       <Button 
        onPress={this.displayData.bind(this)}
        title='Click here to display Data!'>
       </Button>
      </View>
    );
  }

}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
});
