import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  AsyncStorage,
  TextInput,
  TouchableHighlight,

} from 'react-native';

export default class LocalData extends Component {

  constructor(props){
    super(props);
    this.state = {
      name: '',
      phone: '',
    }
    this.clearData = this.clearData.bind(this)
    this.persistData = this.persistData.bind(this)

  }

  persistData(){
    let name = this.state.name
    let phone = this.state.phone

    AsyncStorage.setItem('name', name)
    AsyncStorage.setItem('phone', phone)

    this.setState({
      name: name,
      persistedName: name,
      phone: phone,
      persistedPhone: phone,
    })
  }

  check(){
    AsyncStorage.getItem('name').then((name)=> {
      this.setState({name: name, persistedName: name})
    })

    AsyncStorage.getItem('phone').then((phone)=> {
      this.setState({phone: phone, persistedPhone: phone})
    })
  }

  componentWillMount(){
   this.check();
  }

  clearData(){
    AsyncStorage.clear();
    this.setState({
      persistedName: '',
      persistedPhone: '',
    })

  }

  render() {
    return (
      <View style={styles.container}>
       <Text>
         Persisting Data
       </Text>

       <Text>
         Name
       </Text>
       <TextInput
        style = {styles.inputStyle}
        value = {this.state.name}
        onChangeText = {(text)=> this.setState({name: text})}
       />

      <Text>
         Phone
      </Text>
      <TextInput
        value = {this.state.phone}
        onChangeText = {(text)=> this.setState({phone: text})}
      />

      <TouchableHighlight
        onPress = {this.persistData}>
        <Text>SAVE</Text>

      </TouchableHighlight>

      <TouchableHighlight
        onPress = {this.clearData}>
        <Text>CLEAR</Text>

      </TouchableHighlight>

      <Text>
        STATE
      </Text>
      <Text>
        Name: {this.state.name}
      </Text>
      <Text>
        Phone: {this.state.phone}
      </Text>

      <Text>
        PERSISTENCE DATA
      </Text>
      <Text>
        Name: {this.state.persistedName}
      </Text>
      <Text>
        Phone: {this.state.persistedPhone}
      </Text>

      </View>
    );
  }

}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  inputStyle: {
    
  },
});
