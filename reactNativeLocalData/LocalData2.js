import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  AsyncStorage,

} from 'react-native';

export default class LocalData extends Component {
  render() {
    return (
      <View style={styles.container}>
       <TouchableOpacity 
        onPress={this.saveData}>
         <Text>
           Click here to save Data!
         </Text>
       </TouchableOpacity>

       <TouchableOpacity
        onPress={this.displayData}>
         <Text>
           Click here to display Data!
         </Text>
       </TouchableOpacity>
      </View>
    );
  }

saveData() {
  alert('You clicked to save data into local data!');

  let obj = {
    name: 'Kimhuo',
    email: 'Kimhuo99@gmail.com',
    address: 'Phnom Penh',
  };

  AsyncStorage.setItem('user', JSON.stringify(obj));

}

displayData = async () => {
  try {
    let user = await AsyncStorage.getItem('user');
    let parsed = JSON.parse(user);

    alert(parsed.email);

  } catch(error) {
    alert(error);

  }
}

}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
});
