import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Button,
  AsyncStorage,

} from 'react-native';

export default class LocalData extends Component {
  render() {
    return (
      <View style={styles.container}>
       <Button
        onPress={this.saveData.bind(this)}
        title='Click here!'>
           
       </Button>

       <Text></Text>

       <Button 
        onPress={this.displayData.bind(this)}
        title='Click here to display Data!'>
           
       </Button>
      </View>
    );
  }

saveData() {
  alert('Please, click the text below to display data!');
}

displayData() {
  AsyncStorage.setItem('UID123', JSON.stringify(UID123_object), () => {
    AsyncStorage.mergeItem('UID123', JSON.stringify(UID123_delta), () => {
      AsyncStorage.getItem('UID123', (err, result) => {
        alert(result);
      });
    });
  });
}

}

let UID123_object = {
  name: 'Chris',
  age: 30,
  traits: {hair: 'brown', eyes: 'brown'},
};

// You only need to define what will be added or updated
let UID123_delta = {
age: 31,
traits: {eyes: 'blue', shoe_size: 10}
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
});
