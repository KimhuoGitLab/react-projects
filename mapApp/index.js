import { AppRegistry } from 'react-native';
import App from './MapApp';

AppRegistry.registerComponent('mapApp', () => App);
