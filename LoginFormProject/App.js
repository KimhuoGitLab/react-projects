import React, { Component } from 'react';
import { Button } from 'react-native-elements'
import {
  Platform,
  StyleSheet,
  Text,
  Image,
  TextInput,
  TouchableOpacity,
  Linking,
  WebView,
  Alert,
  ScrollView,
} from 'react-native';

export default class App extends Component {

  //BtnFunction
  _onPressButton() {
    Alert.alert('You tapped the LOG IN button!')
  }

  render() {

    return (
      <ScrollView contentContainerStyle={styles.container} 
        showsVerticalScrollIndicator={false}>
        <Image
          /* style={{width: 200, height: 200}} */
          style={styles.stretch}
          resizeMode="contain"
          source={require('./img/react_nativ_build_mobile_app-img.png')}
        />
        <Text style={styles.welcome}>
          Welcome to React Native Login Form!
        </Text>
        <Text style={styles.instructions}>
          To get started,
        </Text>

        <Text style={styles.instructions}>
          Please input your Username and Password
        </Text>

        <TextInput
          style={styles.txtInput}        
          underlineColorAndroid= 'rgba(0,0,0,0)'
          placeholder='Username'
          keyboardType='email-address'
        />

        <TextInput
          secureTextEntry={true}
          style={styles.txtInput}
          underlineColorAndroid= 'rgba(0,0,0,0)'
          placeholder='Password'
          keyboardType='numeric'
        />
        <Button
          buttonStyle={styles.btnLogin}
          color= "#FFFFFF"
          title='LOG IN'
          onPress={this._onPressButton}
        />
        
        <TouchableOpacity>
          <Text
              style={styles.touchOpa}
              onPress={() => {Linking.openURL('http://www.facebook.com/Kimhuo99')}}
          >
              SIGN UP
          </Text>
        </TouchableOpacity>
      </ScrollView>
    );
  }
}

// StyleSheet
const styles = StyleSheet.create({
  container: {
    // flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 18,
    textAlign: 'center',
    margin: 10,
    fontWeight: 'bold',
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  stretch: {
    width: 200,
    height: 200,
  },
  txtInput: {
    color: 'black',
    width: 260,
    height: 40, 
    borderColor: 'gray', 
    borderWidth: 1,
    margin: 5,
    padding: 10,
    backgroundColor: '#FFFFFF',
  },
  btnLogin: {
    margin: 5,
    padding: 10,
    width: 260,
    backgroundColor: '#2096F3',
  },
  touchOpa: {
    textDecorationLine: 'underline',
    color: '#2096F3',
  }
});
