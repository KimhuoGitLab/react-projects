import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Button,
  Linking,

} from 'react-native';

export default class App extends Component {

  openLink = () => {
    Linking.openURL("linkedApp://detailScreen")
  }

  openFacebook = () => {
    Linking.openURL("fb://profile/571623549548803")
  }

  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.welcome}>
          Welcome to React Native Linking!
        </Text>
        
        <Button
          title='Click here to Open LinkedApp'
          onPress={() => {this.openLink()}}/>

        <Text/>

        <Button
          title='Click here to Open Facebook'
          onPress={() => {this.openFacebook()}}/>

      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});
