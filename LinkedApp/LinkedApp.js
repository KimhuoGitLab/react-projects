import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Linking,

} from 'react-native';

export default class App extends Component {
  
  componentDidMount(){
    if (Platform.OS === 'android') {
      Linking.getInitialURL().then(url => {
        console.log(URL)
        //Handling routing to url
      });
     }

     if (Platform.OS === 'iOS') {
      Linking.addEventListener('url', console.log("iOS Device"));
     }
  }

  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.welcome}>
          Welcome to React Native LinkedApp!
        </Text>
        
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});
