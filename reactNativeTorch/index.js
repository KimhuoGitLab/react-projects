import { AppRegistry } from 'react-native';
import TorchExample from './TorchExample';

AppRegistry.registerComponent('reactNativeTorch', () => TorchExample);
